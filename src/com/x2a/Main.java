package com.x2a;

import com.x2a.solver.*;

/**
 * Created by Ethan on 4/1/2015.
 */
public class Main {




    public static void main(String[] args) {
        Main main = new Main();
        main.run();
    }


    public void run() {
        InitialState s = new InitialState();
        GlobalRules gr = new GlobalRules();

        Solver solver = new Solver(s, gr);

        SolutionGeneratorLoop sl = new SolutionGeneratorLoop();
        sl.generateSolutions();
    }
}
