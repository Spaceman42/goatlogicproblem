package com.x2a.solver.rules;

import com.x2a.solver.Solution;
import com.x2a.solver.Stage;

/**
 * Created by 40501 on 4/2/2015.
 */
public abstract class Rule extends RuleNode {


    private RuleType type;

    public Rule(RuleType type) {
        this.type = type;
    }

    @Override
    protected abstract boolean applyRule(Solution s);

    @Override
    public RuleType getType() {
        return type;
    }


    public abstract Stage getReqStage();
}
