package com.x2a.solver.rules;

import com.x2a.solver.Solution;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 40501 on 4/2/2015.
 */
public abstract class RuleNode {
    private List<RuleNode> rules;

    public RuleNode() {
        rules = new ArrayList<RuleNode>();
    }

    protected abstract boolean applyRule(Solution s);

    public boolean checkSolution(Solution s) {
        applyRule(s);
        for (RuleNode r : rules) {
            boolean result = r.checkSolution(s);

            if (r.getType() == RuleType.NOT) {
                if (result) {
                    return false;
                }
            } else {
                if (!result) {
                    return false;
                }
            }
        }

        return true;
    }


    public void addRule(RuleNode rule) {
        rules.add(rule);
    }

    public boolean removeRule(RuleNode rule) {
        return rules.remove(rule);
    }

    public abstract RuleType getType();
}
