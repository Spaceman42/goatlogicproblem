package com.x2a.solver.rules;

import com.x2a.solver.Solution;

/**
 * Created by 40501 on 4/2/2015.
 */
public class RuleSet extends RuleNode {
    @Override
    protected boolean applyRule(Solution s) {
        return true;
    }

    @Override
    public RuleType getType() {
        return RuleType.FOLLOW;
    }
}
