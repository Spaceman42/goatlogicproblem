package com.x2a.solver.rules.goat;

import com.x2a.solver.Stage;
import com.x2a.solver.rules.RuleSet;

import java.util.*;

/**
 * Created by 40501 on 4/2/2015.
 */
public class GoatRuleSet extends RuleSet {
    private static Stack<Stage> stageStack;

    public GoatRuleSet() {
        addRule(new GenderRule());

    }


    public static void pushStage(Stage s) {
        stageStack.push(s);
    }

    public static void popStage() {
        stageStack.pop();
    }

    public boolean containsStage(Stage s) {
        return stageStack.contains(s);
    }
}
