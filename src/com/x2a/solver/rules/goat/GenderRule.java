package com.x2a.solver.rules.goat;

import com.x2a.solver.Data.FirstName;
import com.x2a.solver.Data.LastName;
import com.x2a.solver.Solution;
import com.x2a.solver.rules.Rule;
import com.x2a.solver.rules.RuleType;
import com.x2a.solver.sim.Gender;
import com.x2a.solver.sim.Person;

import java.util.Collection;
import java.util.Map;

/**
 * Created by 40501 on 4/2/2015.
 *
 * Poorly designed class which makes sure names have the correct genders.
 */
public class GenderRule extends Rule{

    public GenderRule() {
        super(RuleType.FOLLOW);
    }

    @Override
    protected boolean applyRule(Solution s) {
        Collection<Person> people = s.getPeople();

        boolean firstNames = doFirstNames(people);
        boolean lastNames = doLastNames(people);

        return firstNames && lastNames;
    }

    private boolean doLastNames(Collection<Person> people) {
        for (Person p : people) {
            if (isMale(p)) {
                if (p.lastName == LastName.CHOU) {
                    return false;
                }
                if (p.lastName == LastName.SANCHEZ) {
                    return false;
                }
                if (p.lastName == LastName.ZEIGLER) {
                    return false;
                }
                if (p.lastName == LastName.TOWNSEND) {
                    return false;
                }
                if (p.lastName == LastName.VANDERGRIFT) {
                    return false;
                }
            } else {
                if (p.lastName == LastName.ANDERSON) {
                    return false;
                }
                if (p.lastName == LastName.LANDIS) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean doFirstNames(Collection<Person> people) {
        for (Person p : people) {
            if (isMale(p)) {
                if (p.firstName == FirstName.THERESA) {
                    return false;
                }
                if (p.firstName == FirstName.TRACI) {
                    return false;
                }
                if (p.firstName == FirstName.GLORIA) {
                    return false;
                }
                if (p.firstName == FirstName.MONIQUE) {
                    return false;
                }
                if (p.firstName == FirstName.CINDY) {
                    return false;
                }
                if (p.firstName == FirstName.PENNY) {
                    return false;
                }
            } else {
                if (p.firstName == FirstName.BILL) {
                    return false;
                }
                if (p.firstName == FirstName.GREGORY) {
                    return false;
                }
                if (p.firstName == FirstName.RAYMOND) {
                    return false;
                }
                if (p.firstName == FirstName.PIERRE) {
                    return false;
                }
                if (p.firstName == FirstName.TRAVIS) {
                    return false;
                }
                if (p.firstName == FirstName.CHARLES) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean isMale(Person p) {
        return p.gender == Gender.MALE;
    }
}
