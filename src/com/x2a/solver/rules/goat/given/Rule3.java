package com.x2a.solver.rules.goat.given;

import com.x2a.solver.Data.LastName;
import com.x2a.solver.Data.Snack;
import com.x2a.solver.Solution;
import com.x2a.solver.Stage;
import com.x2a.solver.rules.Rule;
import com.x2a.solver.rules.RuleType;
import com.x2a.solver.sim.Person;

/**
 * Created by 40501 on 4/2/2015.
 */
public class Rule3 extends Rule {
    public Rule3() {
        super(RuleType.FOLLOW);
    }

    @Override
    protected boolean applyRule(Solution s) {
        for (Person p : s.getPeople()) {
            if (p.lastName == LastName.DANZA) {
                if (p.snacks.size() != 1) {
                    return false;
                } else {
                    if (!p.snacks.contains(Snack.ICE_CREAM)) {
                        return false;
                    }
                }
            } else if (p.lastName == LastName.ZETESLO) {
                if (p.snacks.contains(Snack.POPCORN) || p.snacks.contains(Snack.COTTON_CANDY)) {
                    return false;
                }
            } else if (p.lastName == LastName.TOWNSEND) {
                if (p.snacks.contains(Snack.SODA)) {
                    return false;
                }
            }
        }

        return true;
    }

    @Override
    public Stage getReqStage() {
        return Stage.PEOPLE_SNACKS;
    }
}
