package com.x2a.solver.rules.goat.given;

import com.x2a.solver.Solution;
import com.x2a.solver.Stage;
import com.x2a.solver.rules.Rule;
import com.x2a.solver.rules.RuleNode;
import com.x2a.solver.rules.RuleType;
import com.x2a.solver.sim.Car;

import java.util.Collection;

/**
 * Created by 40501 on 4/2/2015.
 */
public class Rule1 extends Rule {
    public Rule1() {
        super(RuleType.FOLLOW);
    }

    @Override
    protected boolean applyRule(Solution s) {
        int numberOfPairs = 0;
        for (Car c : s.getCars()) {
            if (containsPair(c)) {
                numberOfPairs++;
            }
        }
        if (numberOfPairs != 5) {
            return false;
        }
        return true;
    }

    @Override
    public Stage getReqStage() {
        return Stage.CAR_PEOPLE;
    }

    private boolean containsPair(Car cars) {
        return cars.person1 != null && cars.person2 != null;
    }
}
