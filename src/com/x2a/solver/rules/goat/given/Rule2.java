package com.x2a.solver.rules.goat.given;

import com.x2a.solver.Data.Snack;
import com.x2a.solver.Solution;
import com.x2a.solver.Stage;
import com.x2a.solver.rules.Rule;
import com.x2a.solver.rules.RuleType;
import com.x2a.solver.sim.Person;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 40501 on 4/2/2015.
 */
public class Rule2 extends Rule {
    public Rule2() {
        super(RuleType.FOLLOW);
    }

    @Override
    protected boolean applyRule(Solution s) {

        int[] numbers = new int[4];
        int numberOfFive = 0;
        int numberWithPartner = 0;
        int i = 0;
        for (Snack sn : Snack.values()) {
            int amount = 0;
            for (Person p : s.getPeople()) {
                if (p.snacks.contains(sn)) {
                    amount ++;
                    if (p.partner.snacks.contains(sn)) {
                        numberWithPartner++;
                    }
                }
            }
            if (amount == 5) {
                numberOfFive++;
            }

            numbers[i] = amount;
            i++;
        }

        if (numberOfFive != 5 || numberWithPartner != 1) {
            return false;
        }


        return threeSameValues(numbers);
    }

    @Override
    public Stage getReqStage() {
        return Stage.PEOPLE_SNACKS;
    }

    /**
     * Checks to see if 3 of the array values are the same. Super crappy method. Might be a way to write it so it runs faster
     * @param values
     * @return
     */
    private boolean threeSameValues(int[] values) {
        int start = values[0];
        int amountSame = 0;
        for (int i : values) {
            if (start == i) {
                amountSame++;
            }
        }

        if (amountSame != 3) {
            int newStart = values[1];
            int amountNewSame = 0;
            for (int i : values) {
                if (newStart == i) {
                    amountNewSame++;
                }
            }

            if (amountNewSame != 3) {
                return false;
            }
        }

        return true;
    }
}
