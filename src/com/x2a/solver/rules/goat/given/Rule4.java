package com.x2a.solver.rules.goat.given;

import com.x2a.solver.Solution;
import com.x2a.solver.Stage;
import com.x2a.solver.rules.Rule;
import com.x2a.solver.rules.RuleType;
import com.x2a.solver.sim.Car;
import com.x2a.solver.sim.Gender;

/**
 * Created by 40501 on 4/2/2015.
 */
public class Rule4 extends Rule{
    public Rule4() {
        super(RuleType.FOLLOW);
    }

    @Override
    protected boolean applyRule(Solution s) {
        int oddGirls = 0;
        int evenGirls = 0;
        int oddBoys = 0;
        int evenBoys = 0;

        for (Car c : s.getCars()) {
            if (c.number % 2 == 0) { //even carts
                if (c.person1 != null) {
                    if (c.person1.gender == Gender.MALE) {
                        evenBoys++;
                    } else {
                        evenGirls++;
                    }
                }
                if (c.person2 != null) {
                    if (c.person2.gender == Gender.MALE) {
                        evenBoys++;
                    } else {
                        evenGirls++;
                    }
                }
            } else { //odd cars
                if (c.person1 != null) {
                    if (c.person1.gender == Gender.MALE) {
                        oddBoys++;
                    } else {
                        oddGirls++;
                    }
                }
                if (c.person2 != null) {
                    if (c.person2.gender == Gender.MALE) {
                        oddBoys++;
                    } else {
                        oddGirls++;
                    }
                }
            }
        }

        if ((oddBoys+oddGirls > evenBoys+evenGirls) || oddGirls<evenGirls) {
            return false;
        }

        return true;
    }

    @Override
    public Stage getReqStage() {
        return Stage.CAR_PEOPLE;
    }
}
