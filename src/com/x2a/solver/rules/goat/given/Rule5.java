package com.x2a.solver.rules.goat.given;

import com.x2a.solver.Data.CarColor;
import com.x2a.solver.Data.CarType;
import com.x2a.solver.Solution;
import com.x2a.solver.Stage;
import com.x2a.solver.rules.Rule;
import com.x2a.solver.rules.RuleType;
import com.x2a.solver.sim.Car;

/**
 * Created by 40501 on 4/2/2015.
 */
public class Rule5 extends Rule {
    public Rule5() {
        super(RuleType.FOLLOW);
    }

    @Override
    protected boolean applyRule(Solution s) {

        for (Car c : s.getCars()) {
            if (c.number == 0) {
                if (c.type == CarType.LION) {
                    return false;
                }
            } else if (c.number == 1) {
                if (c.type != CarType.DRAGON || c.color != CarColor.RED) {
                    return false;
                }
            } else if (c.number == 2) {
                if (c.type != CarType.HORSE) {
                    return false;
                }
            } else if (c.number == 3) {
                if (c.riderNumber() != 1) {
                    return false;
                }
            }
        }

        return false;
    }

    @Override
    public Stage getReqStage() {
        return Stage.CAR_PEOPLE;
    }
}
