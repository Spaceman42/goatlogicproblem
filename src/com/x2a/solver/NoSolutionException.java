package com.x2a.solver;

/**
 * Created by Ethan on 4/1/2015.
 */
public class NoSolutionException extends Exception {

    public NoSolutionException(String msg) {
        super(msg);
    }
}
