package com.x2a.solver;

import com.x2a.solver.Data.FirstName;
import com.x2a.solver.Data.LastName;
import com.x2a.solver.sim.Car;
import com.x2a.solver.sim.Person;

import java.util.Collection;
import java.util.Map;

/**
 * Created by Ethan on 4/1/2015.
 */
public class Solution {


    public Collection<Person> getPeople() {
        return null;
    }

    public Map<FirstName, Person> getPeopleByFirstName() {
        return null;
    }

    public Map<LastName, Person> getPeopleByLastName() {
        return null;
    }

    public Collection<Car> getCars() {
        return null;
    }
}
