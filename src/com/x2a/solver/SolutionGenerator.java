package com.x2a.solver;

import com.x2a.solver.Data.CarColor;
import com.x2a.solver.Data.CarType;
import com.x2a.solver.sim.Car;

import java.util.*;

/**
 * Created by Ethan on 4/1/2015.
 */
public class SolutionGenerator {

    private Stack<CarColor> colorToUse;
    private Stack<CarType> typeToUse;

    public void generateSolution(Queue<Solution> outputQueue) {
        List<CarColor> colors = Arrays.asList(CarColor.values());


    }


    public void generateCartsColor() {
        List<CarColor> colors = new ArrayList<CarColor>(Arrays.asList(CarColor.values()));
        colorToUse = new Stack<CarColor>();
        System.out.println("About to start. Color size: " + colors.size());
        innerColor(colors, colorToUse);
    }

    private void innerColor(List<CarColor> colors, Stack<CarColor> colorToUse) {

        if (colors.size() != 0) {
            for (int i = 0; i<colors.size(); i++) {
                CarColor colorNext = colors.get(i);
                colors.remove(colorNext);
                colorToUse.push(colorNext);
                innerColor(colors, colorToUse);
                colorToUse.pop();
            }
        }
        generateCartsType();
        //Do next loop or send to queue
    }


    public void generateCartsType() {
        List<CarType> types = new ArrayList<CarType>(Arrays.asList(CarType.values()));
        typeToUse = new Stack<CarType>();
        System.out.println("type size: " + types.size());
        innerType(types, typeToUse);
    }

    private void innerType(List<CarType> types, Stack<CarType> typeToUse) {

        if (types.size() != 0) {
            List<CarType> newTypes = new ArrayList<CarType>(types);
            if (!typeToUse.isEmpty()) {
                newTypes.remove(typeToUse.peek());
            }
            for (CarType t : newTypes) {
                typeToUse.push(t);
                innerType(newTypes, typeToUse);
                typeToUse.pop();
            }
        }

        System.out.println(" ");

        Car[] cars = getCarArray();

        for (int i = 0; i < 10; i++) {
            cars[i].color = colorToUse.get(i);
            cars[i].type = typeToUse.get(i);


            System.out.print(cars[i] + ", ");
        }
        System.out.println("Finished with list one");


    }



    private Car[] getCarArray() {
        Car[] cars = new Car[10];

        for (int i = 0; i < 10; i++) {
            cars[i] = new Car();
            cars[i].number = i;
        }

        return cars;
    }

}
