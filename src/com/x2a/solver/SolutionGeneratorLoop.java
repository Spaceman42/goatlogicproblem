package com.x2a.solver;

import com.x2a.PermArrayList;
import com.x2a.PermIterator;
import com.x2a.solver.Data.CarColor;
import com.x2a.solver.Data.CarType;
import com.x2a.solver.sim.Car;

import java.lang.reflect.Array;
import java.util.*;

/**
 * Created by Ethan on 4/1/2015.
 */
public class SolutionGeneratorLoop {


    public void generateSolutions() {
        List<CarColor> colors = new PermArrayList<CarColor>();
        colors.addAll(Arrays.asList(CarColor.values()));

        PermIterator<CarColor> colorPermIt = new PermIterator<CarColor>(colors);
        System.out.println("running");

        long currentTime = System.nanoTime();

        long differentArrays = 0L;

        while(colorPermIt.hasNext()) {
            List<CarType> types = new PermArrayList<CarType>();
            types.addAll(Arrays.asList(CarType.values()));

            PermIterator<CarType> typePermIt = new PermIterator<CarType>(types);
            List<CarColor> permColor = colorPermIt.next();
            while(typePermIt.hasNext()) {
                List<CarType> permType = typePermIt.next();

                differentArrays++;

                if (differentArrays % 100000000 == 0) {
                    System.out.println((double)differentArrays/(3628800d*3628800d));
                }

                //Car[] cars = getCarArray();

                //for (int i = 0; i < 10; i++) {
                 //   cars[i].color = permColor.get(i);
                //    cars[i].type = permType.get(i);
               // }


            }

        }

        long newTime = System.nanoTime();

        long deltaT = Math.abs(newTime - currentTime);

        double seconds = deltaT/1000000000.0d;
        System.out.println("Finished with list. Took: " + seconds +". Generate " + differentArrays +" different arrays");

    }



    private Car[] getCarArray() {
        Car[] cars = new Car[10];

        for (int i = 0; i < 10; i++) {
            cars[i] = new Car();
            cars[i].number = i;
        }

        return cars;
    }
}
