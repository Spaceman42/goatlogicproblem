package com.x2a.solver;

import java.util.List;

/**
 * Created by Ethan on 4/1/2015.
 */
public class Solver {

    private InitialState initialState;

    private GlobalRules globalRules;


    public Solver(InitialState initialState, GlobalRules globalRules) {
        this.initialState = initialState;
        this.globalRules = globalRules;
    }

    public Solution solve() throws NoSolutionException {
        boolean solved = false;

        long iteration = 0;

        while(true) {
            buildInitialSets();
            trimSets();

            List<Solution> solutions = buildPotentialSolutions();
            trimSetsGlobal(solutions);

            if (solutions.size() == 1) {
                return solutions.get(0);
            }

            if (solutions.size() < 1) {
                throw new NoSolutionException("Failed on iteration " + iteration);
            }
            iteration++;
        }
    }


    private void buildInitialSets() {
        //Fill initial unfilled lists with potential info
    }

    private void trimSets() {
        //Trim sets based on other potential sets
    }

    private List<Solution> buildPotentialSolutions() {
        return null;
    }

    private void trimSetsGlobal(List<Solution> solutions) {
        for (Solution s : solutions) {
            if (!globalRules.checkSolution(s)) {
                solutions.remove(s);
            }
        }
    }
}
