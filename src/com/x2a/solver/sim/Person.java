package com.x2a.solver.sim;

import com.x2a.solver.Data.FirstName;
import com.x2a.solver.Data.LastName;
import com.x2a.solver.Data.Snack;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Ethan on 4/1/2015.
 */
public class Person {
    public Car car;

    public Person partner;

    public Set<Snack> snacks = new HashSet<Snack>();

    public Gender gender;

    public FirstName firstName;

    public LastName lastName;
}
