package com.x2a.solver.sim;

import com.x2a.solver.Data.CarColor;
import com.x2a.solver.Data.CarType;

/**
 * Created by Ethan on 4/1/2015.
 */
public class Car {
    public CarType type;
    public CarColor color;

    public int number;

    public Person person1;
    public Person person2;


    @Override
    public String toString() {
        return "Car #" + number + ": " + color +" " + type;
    }

    public int riderNumber() {
        int amount = 0;
        if (person1 != null) {
            amount++;
        }
        if (person2 != null) {
            amount++;
        }
        return amount;
    }
}
