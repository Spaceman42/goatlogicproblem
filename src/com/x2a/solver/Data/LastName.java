package com.x2a.solver.Data;

/**
 * Created by Ethan on 4/1/2015.
 */
public enum LastName {
    ALTMAN, ANDERSON, CHOU, COLLINS, DANZA, LANDIS, SANCHEZ, TOWNSEND, VANDERGRIFT, ZEIGLER, ZETESLO, MARTINI;
}
