package com.x2a.solver.Data;

/**
 * Created by Ethan on 4/1/2015.
 */
public enum CarColor {
    RED, ORANGE, BLUE, YELLOW, PURPLE, BLACK, PINK, SILVER, GREEN, WHITE;
}
