package com.x2a.solver.Data;

/**
 * Created by Ethan on 4/1/2015.
 */
public enum FirstName {
    BILL, RAYMOND, THERESA, TRAVIS, MONIQUE, PENNY, PIERRE, GLORIA, CINDY, TRACI, CHARLES, GREGORY;
}
