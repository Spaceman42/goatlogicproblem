package com.x2a.solver.Data;

/**
 * Created by Ethan on 4/1/2015.
 */
public enum Snack {
    ICE_CREAM, COTTON_CANDY, POPCORN, SODA;
}
