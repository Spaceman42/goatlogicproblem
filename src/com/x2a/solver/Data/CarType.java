package com.x2a.solver.Data;

/**
 * Created by Ethan on 4/1/2015.
 */
public enum CarType {
    GOAT(true), DRAGON(false), LION(true), CAT(true), DOG(true), ROOSTER(false), EAGLE(false), FOX(true), HORSE(true), PIG(true);


    public final boolean mammal;

    CarType(boolean mammal) {
        this.mammal = mammal;
    }
}
