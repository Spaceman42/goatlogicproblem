package com.x2a;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Ethan on 4/1/2015.
 */
public class PermArrayList<E> extends ArrayList<E> {


    @Override
    public Iterator<E> iterator() {
        return (Iterator<E>)new PermIterator<E>(this);
    }
}
